/**
 * @file
 * SCEditor implementation of {@link Drupal.editors} API.
 */

((Drupal, sceditor, $) => {
  /**
   * Integration of SCEditor with the Drupal editor API.
   *
   * @namespace
   *
   * @see Drupal.editorAttach
   */
  Drupal.editors.sceditor = {
    /**
     * Editor attach callback.
     *
     * @param {HTMLElement} element
     *   The element to attach the editor to.
     * @param {string} format
     *   The text format for the editor.
     */
    attach(element, format) {
      // ToDo Add configurable toolbar.
      // ToDo Load style locally.

      sceditor.create(element, {
        format: 'bbcode',
        icons: 'monocons',
        style:
          'https://cdn.jsdelivr.net/npm/sceditor@3/minified/themes/content/default.min.css',
        toolbar:
          'bold,italic,underline,strike|image,link,unlink|bulletlist,orderedlist|quote,code|source',
        autoUpdate: true,
        height: '250px',
      });

      // Update contents of original textarea.
      sceditor.instance(element).bind('keypress', function updateTextarea(e) {
        element.setAttribute('data-editor-value-is-changed', 'true');

        // Unbind keypress for better performance.
        sceditor.instance(element).unbind('keypress', updateTextarea);
      });
    },

    /**
     * Editor detach callback.
     *
     * @param {HTMLElement} element
     *   The element to detach the editor from.
     * @param {string} format
     *   The text format used for the editor.
     * @param {string} trigger
     *   The event trigger for the detach.
     */
    detach(element, format, trigger) {
      if (trigger !== 'serialize') {
        sceditor.instance(element).destroy();
      }
    },

    /**
     * Registers a callback which SCEditor will call on change:data event.
     *
     * @param {HTMLElement} element
     *   The element where the change occurred.
     * @param {function} callback
     *   Callback called with the value of the editor.
     */
    onChange(element, callback) {},
  };
})(Drupal, sceditor, jQuery);
